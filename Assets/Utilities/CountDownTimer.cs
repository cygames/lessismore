﻿using System;
using System.Collections;
using UnityEngine;

class CountDownTimer : MonoBehaviour
{
    public static event Action<TimerStartData> OnTimerStart;
    public static event Action OnTimerEnd;
    public static event Action<TimerTickData> OnTimerTick;

    private readonly WaitForSeconds waitForSeconds = new WaitForSeconds(0.1f);

    public float TimeRemaining { get; private set; }
    public float TotalTime { get; private set; }

    private CountDownTimer() { /* Non-instantiable */ }

    public void StartTimer(float totalTime)
    {
        StartCoroutine(Timeout(totalTime));
    }

    public void StopTimer()
    {
        StopAllCoroutines();
    }

    public void AdjustTimeRemaining(float adjustment)
    {
        TimeRemaining += adjustment;
    }

    public void ResetTimer()
    {
        TimeRemaining = TotalTime;
    }

    private IEnumerator Timeout(float totalTime)
    {
        var previousTime = Time.time;

        TimeRemaining = totalTime;
        TotalTime = totalTime;

        OnTimerStart?.Invoke(new TimerStartData(totalTime));
        while ((TimeRemaining -= Time.time - previousTime) > 0)
        {
            previousTime = Time.time;
            OnTimerTick?.Invoke(new TimerTickData(totalTime, TimeRemaining));
            yield return waitForSeconds;
        }

        OnTimerEnd?.Invoke();
    }

    public struct TimerStartData
    {
        public TimerStartData(float totalTime)
        {
            TotalTime = totalTime;
        }

        public float TotalTime { get; private set; }
    }

    public struct TimerTickData
    {
        public TimerTickData(float totalTime, float timeRemaining)
        {
            TotalTime = totalTime;
            TimeRemaining = timeRemaining;
        }

        public float TotalTime { get; private set; }
        public float TimeRemaining { get; private set; }
    }
}
