﻿using System;
using System.Collections.Generic;
using UnityEngine;

class HighScoresManager: MonoBehaviour
{
    [SerializeField] private int numberOfHighScores = 5;

    private IDictionary<Category, IList<HighScore>> highScores;

    public IList<HighScore> GetScores(Category category)
    {
        if (null == highScores)
        {
            LoadHighScores();
        }
        return highScores[category];
    }

    public bool CheckHighScore(Category category, int score)
    {
        foreach(var highScore in GetScores(category))
        {
            if (highScore.Score <= score)
            {
                return true;
            }
        }
        return false;
    }

    public void AddHighScore(Category category, HighScore score)
    {
        var categoryHighScores = highScores[category];
        for (var i = 0; i < categoryHighScores.Count; i++)
        {
            var highScore = categoryHighScores[i];
            if (highScore.Score <= score.Score)
            {
                InsertHighScore(category, score, i);
                StoreCurrentHighScores(category);
                return;
            }
        }

        throw new InvalidOperationException("Score is not a hiigh score");
    }

    private void InsertHighScore(Category category, HighScore score, int index)
    {
        MoveHighScoresDownFromIndex(category, index);
        highScores[category][index] = score;
    }

    private void MoveHighScoresDownFromIndex(Category category, int index)
    {
        var categoryHighScores = GetScores(category);
        for (var i = categoryHighScores.Count - 1; i > index; i--)
        {
            categoryHighScores[i] = categoryHighScores[i - 1];
        }
    }

    private void StoreCurrentHighScores(Category category)
    {
        for (var i = 0; i < numberOfHighScores; i++)
        {
            StoreCurrentHighScore(category, i);
        }
    }

    private void StoreCurrentHighScore(Category category, int index)
    {
        var keyPrefix = $"{category}_{index}";
        var score = GetScores(category)[index];
        PlayerPrefs.SetString($"{keyPrefix}_NAME", score.Name);
        PlayerPrefs.SetInt($"{keyPrefix}_SCORE", score.Score);
    }

    private void LoadHighScores()
    {
        highScores = new Dictionary<Category, IList<HighScore>>();

        foreach (Category category in Enum.GetValues(typeof(Category)))
        {
            highScores.Add(category, new HighScore[numberOfHighScores]);
            for (int i = 0; i < numberOfHighScores; i++)
            {
                LoadHighScore(category, i);
            }
        }
    }

    private void LoadHighScore(Category category, int index)
    {
        var keyPrefix = $"{category}_{index}";

        var highScore = HighScore.EMPTY_SCORE;
        if (PlayerPrefs.HasKey($"{keyPrefix}_NAME"))
        {
            var name = PlayerPrefs.GetString($"{keyPrefix}_NAME");
            var score = PlayerPrefs.GetInt($"{keyPrefix}_SCORE");
            highScore = HighScore.of(name, score);
        }

        highScores[category][index] = highScore;
    }
}

public class HighScore
{
    public static readonly HighScore EMPTY_SCORE = of("---", 0);

    public string Name { get; private set; }
    public int Score { get; private set; }

    private HighScore(string name, int score)
    {
        Name = name;
        Score = score;
    }

    public static HighScore of(string name, int score)
    {
        return new HighScore(name, score);
    }

    public override string ToString()
    {
        return $"HighScore ({Name}, {Score})";
    }
}

public enum Category
{
    TIMED,
    MARATHON
}
