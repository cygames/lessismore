using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectUtils
{
    private GameObjectUtils() { /* Non-instantiable */ }

    public static void DestroyChildren(Transform obj)
    {
        for (var i = 0; i < obj.childCount; i++)
        {
            Object.DestroyImmediate(obj.GetChild(i).gameObject);
        }
    }

    public static void PopulateWithTagIfNull(ref Transform output, string tag)
    {
        if (null == output)
        {
            output = GameObject.FindWithTag(tag).transform;
        }
    }

    public static T GetComponentWithTag<T>(string tag)
    {
        var gameObjects = GameObject.FindGameObjectsWithTag(tag);
        foreach (var go in gameObjects)
        {
            var component = go.GetComponent<T>();
            if (null != component)
            {
                return component;
            }
        }

        return default;
    }
}
