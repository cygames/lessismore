using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InputControls : MonoBehaviour
{
    [SerializeField] private Transform gameModeHudTransform;
    [SerializeField] private Transform leftButtonTransform;
    [SerializeField] private Transform rightButtonTransform;

    private Button leftButton;
    private Button rightButton;

    private AnswerChecker answerChecker;
    private UnityAction checkAnswerLeft;
    private UnityAction checkAnswerRight;

    private TMPro.TextMeshProUGUI gameModeText;

    private bool inputEnabled;
    private bool axisMovementRegistered;

    void Awake()
    {
        GameObjectUtils.PopulateWithTagIfNull(ref leftButtonTransform, "LeftButton");
        GameObjectUtils.PopulateWithTagIfNull(ref rightButtonTransform, "RightButton");
        GameObjectUtils.PopulateWithTagIfNull(ref gameModeHudTransform, "GameModeHUD");

        leftButton = leftButtonTransform.GetComponent<Button>();
        rightButton = rightButtonTransform.GetComponent<Button>();
        gameModeText = gameModeHudTransform.GetComponent<TMPro.TextMeshProUGUI>();

        inputEnabled = true;
    }

    private void Update()
    {
        HandleAxisInput();
    }

    private void HandleAxisInput()
    {
        if (!inputEnabled) return;

        var axisMovement = Input.GetAxisRaw("Horizontal");
        if (0 != axisMovement)
        {
            if (!axisMovementRegistered)
            {
                answerChecker.CheckAnswer(0 > axisMovement);
                axisMovementRegistered = true;
            }
        }
        else
        {
            axisMovementRegistered = false;
        }
    }

    internal void Enable() => SetInputEnabled(true);
    internal void Disable() => SetInputEnabled(false);

    internal Tuple<float, float> GetValues()
    {
        var leftValue = leftButtonTransform.GetComponentInChildren<Value>().GetValue();
        var rightValue = rightButtonTransform.GetComponentInChildren<Value>().GetValue();

        return new Tuple<float, float>(leftValue, rightValue);
    }

    internal void DestroyValues()
    {
        GameObjectUtils.DestroyChildren(leftButtonTransform);
        GameObjectUtils.DestroyChildren(rightButtonTransform);
    }

    internal Tuple<GameObject, GameObject> InstantiateValues(GameObject prefab)
    {
        var leftPresenter = Instantiate(prefab, leftButtonTransform);
        var rightPresenter = Instantiate(prefab, rightButtonTransform);

        UpdateGameModeHud();

        return new Tuple<GameObject, GameObject>(leftPresenter, rightPresenter);
    }

    internal void SetChecker(AnswerChecker answerChecker)
    {
        this.answerChecker = answerChecker;
        ChangeChecker(leftButton, ref checkAnswerLeft, answerChecker, true);
        ChangeChecker(rightButton, ref checkAnswerRight, answerChecker, false);
    }

    internal void UnsetCurrentChecker()
    {
        RemoveCurrentChecker(leftButton, checkAnswerLeft);
        RemoveCurrentChecker(rightButton, checkAnswerRight);
    }

    private void SetInputEnabled(bool isEnabled)
    {
        inputEnabled = isEnabled;
        leftButton.interactable = isEnabled;
        rightButton.interactable = isEnabled;
    }

    private void ChangeChecker(Button button, ref UnityAction action, AnswerChecker answerChecker, bool isLeft)
    {
        RemoveCurrentChecker(button, action);
        action = () => answerChecker.CheckAnswer(isLeft);

        button.onClick.AddListener(action);
    }

    private void RemoveCurrentChecker(Button button, UnityAction action)
    {
        if (null != action)
        {
            button.onClick.RemoveListener(action);
        }
    }

    private void UpdateGameModeHud()
    {
        if (null == answerChecker)
        {
            Debug.Log("Answer Checker was not populated");
            return;
        }

        gameModeText.SetText(answerChecker.GetGameModeDescription());
        gameModeText.color = answerChecker.GetGameModeColor();

        UpdateTextComponentsColors(leftButtonTransform);
        UpdateTextComponentsColors(rightButtonTransform);
    }

    private void UpdateTextComponentsColors(Transform transform)
    {
        foreach (var text in transform.GetComponentsInChildren<TMPro.TextMeshProUGUI>())
        {
            text.color = answerChecker.GetGameModeColor();
        }
    }
}
