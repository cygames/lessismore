using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[DisallowMultipleComponent]
public abstract class AnswerChecker : MonoBehaviour
{
    public static event Action<bool> OnAnswer;

    [SerializeField] protected InputControls inputControls;

    private void Awake()
    {
        inputControls = GameObjectUtils.GetComponentWithTag<InputControls>("Manager");
    }

    private void OnEnable()
    {
        inputControls.SetChecker(this);
    }

    private void OnDisable()
    {
        inputControls.UnsetCurrentChecker();
    }

    internal abstract string GetGameModeDescription();
    internal abstract Color GetGameModeColor();
    protected abstract bool IsCorrectAnswer(float leftValue, float rightValue, bool pickedLeft);

    public void CheckAnswer(bool pickedLeft)
    {
        var (leftValue, rightValue) = inputControls.GetValues();

        var answer = IsCorrectAnswer(leftValue, rightValue, pickedLeft);
        OnAnswer?.Invoke(answer);
    }
}
