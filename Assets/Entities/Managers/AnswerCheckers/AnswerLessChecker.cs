﻿using UnityEngine;

public class AnswerLessChecker : AnswerChecker
{
    protected override bool IsCorrectAnswer(float leftValue, float rightValue, bool pickedLeft)
    {
        return (leftValue < rightValue) == pickedLeft;
    }

    internal override Color GetGameModeColor()
    {
        return Color.yellow;
    }

    internal override string GetGameModeDescription()
    {
        return "< ≠ >";
    }
}
