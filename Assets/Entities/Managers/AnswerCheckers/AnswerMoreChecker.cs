using UnityEngine;

public class AnswerMoreChecker : AnswerChecker
{
    protected override bool IsCorrectAnswer(float leftValue, float rightValue, bool pickedLeft)
    {
        return (leftValue > rightValue) == pickedLeft;
    }

    internal override Color GetGameModeColor()
    {
        return Color.green;
    }

    internal override string GetGameModeDescription()
    {
        return "< = >";
    }
}
