using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighScoresScreenManager : MonoBehaviour
{
    [SerializeField] private Transform timedSectionScores;
    [SerializeField] private Transform marathonSectionScores;

    private HighScoresManager highScoresManager;

    private void OnEnable()
    {
        highScoresManager = GetComponent<HighScoresManager>();
        PopulateTimedScores();
        PopulateMarathonScores();
    }

    public void ExitToMainMenu() => SceneManager.LoadSceneAsync("MenuScene");

    private void PopulateTimedScores()
    {
        IList<HighScore> timedScores = highScoresManager.GetScores(Category.TIMED);
        Debug.Log("timedScores: " + string.Join(" | ", timedScores));
        PopulateScores(timedScores, timedSectionScores);
    }

    private void PopulateMarathonScores()
    {
        IList<HighScore> marathonScores = highScoresManager.GetScores(Category.MARATHON);
        Debug.Log("marathonScores: " + string.Join(" | ", marathonScores));
        PopulateScores(marathonScores, marathonSectionScores);
    }

    private void PopulateScores(IList<HighScore> scores, Transform section)
    {
        for (var i = 0; i < scores.Count; i++)
        {
            FindTextField(section, $"Score{i + 1}/Player").SetText(scores[i].Name);
            FindTextField(section, $"Score{i + 1}/Score").SetText(scores[i].Score.ToString());
        }
    }

    private TMPro.TextMeshProUGUI FindTextField(Transform section, string path)
    {
        return section.Find(path).GetComponent<TMPro.TextMeshProUGUI>();
    }
}
