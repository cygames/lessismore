using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{

    public void PlayTimed(/*TODO should accept scriptable object with managers' prefabs to use*/)
    {
        PlayerPrefs.SetString("FlavorManagers", "GameFlavors/TimedFlavorManagers");
        SceneManager.LoadSceneAsync("GameScene");
    }

    public void PlayMarathon(/*TODO should accept scriptable object with managers' prefabs to use*/)
    {
        PlayerPrefs.SetString("FlavorManagers", "GameFlavors/MarathonFlavorManagers");
        SceneManager.LoadSceneAsync("GameScene");
    }

    public void OpenHighScores()
    {
        SceneManager.LoadSceneAsync("HighScoresScene");
    }

    public void OpenHowToPlay()
    {
        SceneManager.LoadSceneAsync("HowToPlayScene");
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }

}
