using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextPairPicker : MonoBehaviour
{
    public static event Action<GeneratedValuesPair> OnValuePairGenerated;

    [SerializeField] private InputControls inputControls;
    [SerializeField] private List<GameObject> valuePrefabs = new List<GameObject>();
    [SerializeField] [Min(0.1f)] private float minDifference = 0.1f;

    private void Awake()
    {
        inputControls = GameObjectUtils.GetComponentWithTag<InputControls>("Manager");
    }

    public void GenerateNextPair()
    {
        var index = UnityEngine.Random.Range(0, valuePrefabs.Count);
        var prefab = valuePrefabs[index];

        Value leftValueBehavior;
        Value rightValueBehavior;
        do
        {
            var pair = MakeNewPair(prefab);
            leftValueBehavior = pair.Item1.GetComponentInChildren<Value>();
            rightValueBehavior = pair.Item2.GetComponentInChildren<Value>();
        } while (!IsValidPair(leftValueBehavior, rightValueBehavior));

        GeneratedValuesPair generatedValuesPair
            = new GeneratedValuesPair(leftValueBehavior, rightValueBehavior);
        OnValuePairGenerated?.Invoke(generatedValuesPair);
        Debug.Log("Generated pair=" + generatedValuesPair);
    }

    private bool IsValidPair(Value leftValueBehavior, Value rightValueBehavior)
    {
        float leftValue = leftValueBehavior.GetValue();
        float rightValue = rightValueBehavior.GetValue();
        return Math.Abs(leftValue - rightValue) >= minDifference;
    }

    private Tuple<GameObject, GameObject> MakeNewPair(GameObject prefab)
    {
        inputControls.DestroyValues();
        return inputControls.InstantiateValues(prefab);
    }

    public struct GeneratedValuesPair
    {
        public GeneratedValuesPair(Value left, Value right)
        {
            Left = left;
            Right = right;
        }

        public Value Left { get; private set; }
        public Value Right { get; private set; }
    }
}
