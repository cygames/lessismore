using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Infinite game flavor for testing purposes
public class NoGameOverChecker : GameOverChecker
{
    public override Category Category
    {
        get => throw new System.NotImplementedException();
        protected set => throw new System.NotImplementedException();
    }

    protected override void HandleAnswer(bool isCorrectAnswer) => PublishOnGameNotOverEventWithAnswer(isCorrectAnswer);
}
