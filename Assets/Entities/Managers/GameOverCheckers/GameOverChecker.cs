using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public abstract class GameOverChecker : MonoBehaviour
{
    public abstract Category Category { get; protected set; }

    public static event System.Action OnGameOver;
    public static event System.Action<GameNotOverEvent> OnGameNotOver;

    protected abstract void HandleAnswer(bool isCorrectAnswer);

    protected virtual void OnEnable()
    {
        AnswerChecker.OnAnswer += HandleAnswer;
    }

    protected virtual void OnDisable()
    {
        AnswerChecker.OnAnswer -= HandleAnswer;
    }

    protected void PublishOnGameOverEvent()
    {
        OnGameOver?.Invoke();
    }

    protected void PublishOnGameNotOverEvent(GameNotOverEvent gameNotOverEvent)
    {
        OnGameNotOver?.Invoke(gameNotOverEvent);
    }

    protected void PublishOnGameNotOverEventWithAnswer(bool answer)
    {
        PublishOnGameNotOverEvent(new GameNotOverEvent(answer));
    }

    public class GameNotOverEvent
    {
        public bool AnswerWasCorrect { get; private set; }

        public GameNotOverEvent(bool answerWasCorrect)
        {
            AnswerWasCorrect = answerWasCorrect;
        }
    }
}
