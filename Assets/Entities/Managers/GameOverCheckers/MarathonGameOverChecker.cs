using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarathonGameOverChecker : GameOverChecker
{
    [SerializeField] private float durationInSeconds = 2f;
    [SerializeField] private Transform hud;

    public override Category Category { get; protected set; } = Category.MARATHON;

    private TMPro.TextMeshProUGUI timerText;
    private CountDownTimer countDownTimer;

    private void Awake()
    {
        countDownTimer = GameObjectUtils.GetComponentWithTag<CountDownTimer>("Manager");
        GameObjectUtils.PopulateWithTagIfNull(ref hud, "HUD");

        var timerGO = Instantiate(new GameObject("TimeoutTimerText"), hud);
        timerText = timerGO.AddComponent<TMPro.TextMeshProUGUI>();
        timerText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Center;
        timerText.verticalAlignment = TMPro.VerticalAlignmentOptions.Middle;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        NextPairPicker.OnValuePairGenerated += HandleValuePairGenerated;
        CountDownTimer.OnTimerTick += UpdateTimerHud;
        CountDownTimer.OnTimerEnd += OnTimerEnd;
        countDownTimer.StartTimer(durationInSeconds);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        NextPairPicker.OnValuePairGenerated -= HandleValuePairGenerated;
        StopAllCoroutines();
        countDownTimer.StopTimer();
    }

    protected override void HandleAnswer(bool isCorrectAnswer)
    {
        if (isCorrectAnswer)
        {
            countDownTimer.ResetTimer();
            PublishOnGameNotOverEventWithAnswer(isCorrectAnswer);
        }
        else
        {
            StopAllCoroutines();
            countDownTimer.StopTimer();
            timerText.SetText("0");
            PublishOnGameOverEvent();
        }
    }

    private void HandleValuePairGenerated(NextPairPicker.GeneratedValuesPair valuePair)
    {
        countDownTimer.ResetTimer();
    }

    private void UpdateTimerHud(CountDownTimer.TimerTickData data)
    {
        timerText.SetText(countDownTimer.TimeRemaining.ToString("F1"));
    }

    private void OnTimerEnd()
    {
        timerText.SetText("0");
        PublishOnGameOverEvent();
    }
}
