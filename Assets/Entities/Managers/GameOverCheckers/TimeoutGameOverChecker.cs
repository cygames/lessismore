using System;
using System.Collections;
using UnityEngine;

public class TimeoutGameOverChecker : GameOverChecker
{
    [SerializeField] private int durationInSeconds = 20;
    [SerializeField] private float timeBonusOnCorrectAnswer = 0.5f;
    [SerializeField] private float timePenaltyOnWrongAnswer = 2f;
    [SerializeField] private Transform hud;

    public override Category Category { get; protected set; } = Category.TIMED;

    private CountDownTimer countDownTimer;
    private TMPro.TextMeshProUGUI timerText;

    private void Awake()
    {
        countDownTimer = GameObjectUtils.GetComponentWithTag<CountDownTimer>("Manager");
        GameObjectUtils.PopulateWithTagIfNull(ref hud, "HUD");
        var timerGO = Instantiate(new GameObject("TimeoutTimerText"), hud);
        timerText = timerGO.AddComponent<TMPro.TextMeshProUGUI>();
        timerText.horizontalAlignment = TMPro.HorizontalAlignmentOptions.Center;
        timerText.verticalAlignment = TMPro.VerticalAlignmentOptions.Middle;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        CountDownTimer.OnTimerTick += UpdateTimerHud;
        CountDownTimer.OnTimerEnd += OnTimerEnd;
        countDownTimer.StartTimer(durationInSeconds);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        CountDownTimer.OnTimerTick -= UpdateTimerHud;
        CountDownTimer.OnTimerEnd -= OnTimerEnd;
        countDownTimer.StopTimer();
    }

    protected override void HandleAnswer(bool isCorrectAnswer)
    {
        var timeoutDelta = isCorrectAnswer ? timeBonusOnCorrectAnswer : -timePenaltyOnWrongAnswer;

        countDownTimer.AdjustTimeRemaining(timeoutDelta);

        PublishOnGameNotOverEventWithAnswer(isCorrectAnswer);
    }

    private void UpdateTimerHud(CountDownTimer.TimerTickData data)
    {
        timerText.SetText(countDownTimer.TimeRemaining.ToString("F1"));
    }

    private void OnTimerEnd()
    {
        timerText.SetText("0");
        PublishOnGameOverEvent();
    }
}
