using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeManager : MonoBehaviour
{
    [SerializeField] private AnswerChecker[] answerCheckers;
    [SerializeField] [Range(0, 1)] private float chanceOfModeChange = 0.3f;
    [SerializeField] private int minModeStreak = 2;

    private int modeStreak = -1;
    private int currentAnswerChecker;

    private void Start()
    {
        currentAnswerChecker = 0;
        UpdateAnswerChecker(true);
    }

    internal void HandleCheckerMode()
    {
        modeStreak++;
        if (ShouldChangeMode())
        {
            modeStreak = 0;
            UpdateAnswerChecker(false);
            currentAnswerChecker = (currentAnswerChecker + 1) % answerCheckers.Length;
            UpdateAnswerChecker(true);
        }
    }

    private bool ShouldChangeMode() =>
        modeStreak >= minModeStreak
        && UnityEngine.Random.Range(0f, 1) < chanceOfModeChange;

    private void UpdateAnswerChecker(bool isEnabled) =>
        answerCheckers[currentAnswerChecker].enabled = isEnabled;
}
