using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI text;

    public int Score { get; private set; }  = 0;

    void OnEnable()
    {
        AnswerChecker.OnAnswer += CheckScoredPoint;
    }

    private void Start()
    {
        text?.SetText("0");
    }

    void OnDisable()
    {
        AnswerChecker.OnAnswer -= CheckScoredPoint;
    }

    private void CheckScoredPoint(bool correctAnswer)
    {
        if (correctAnswer)
        {
            Score++;
            text?.SetText(Score.ToString());
        }
    }
}
