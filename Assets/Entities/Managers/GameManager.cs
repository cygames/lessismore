using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject gameOverButtons;
    [SerializeField] private GameObject highScorePanel;

    private GameModeManager gameModeManager;
    private NextPairPicker nextPairPicker;
    private InputControls inputControls;
    private ScoreKeeper scoreKeeper;
    private HighScoresManager highScoresManager;

    private void Awake()
    {
        var flavorManager = PlayerPrefs.GetString("FlavorManagers");
        if (null != flavorManager)
        {
            Instantiate(Resources.Load(flavorManager));
        }
    }

    void OnEnable()
    {
        GameOverChecker.OnGameNotOver += HandleGameNotOver;
        GameOverChecker.OnGameOver += HandleGameOver;
    }

    private void Start()
    {
        const string tag = "Manager";
        inputControls = GameObjectUtils.GetComponentWithTag<InputControls>(tag);
        gameModeManager = GameObjectUtils.GetComponentWithTag<GameModeManager>(tag);
        nextPairPicker = GameObjectUtils.GetComponentWithTag<NextPairPicker>(tag);
        scoreKeeper = GameObjectUtils.GetComponentWithTag<ScoreKeeper>(tag);
        highScoresManager = GameObjectUtils.GetComponentWithTag<HighScoresManager>(tag);

        gameOverButtons.SetActive(false);
        highScorePanel.SetActive(false);

        Next();
    }

    private void OnDisable()
    {
        GameOverChecker.OnGameNotOver -= HandleGameNotOver;
        GameOverChecker.OnGameOver -= HandleGameOver;
    }

    public void Reload() => SceneManager.LoadSceneAsync("GameScene");

    public void ExitToMainMenu() => SceneManager.LoadSceneAsync("MenuScene");

    public void SubmitHighScore()
    {
        var score = scoreKeeper.Score;
        var gameOverChecker = GameObjectUtils.GetComponentWithTag<GameOverChecker>("Manager");

        var nameField = highScorePanel.transform.Find("HighScoreFields/NameField/Text Area/Text");
        var name = nameField.GetComponent<TMPro.TextMeshProUGUI>().text;

        var highScore = HighScore.of(name, score);
        highScoresManager.AddHighScore(gameOverChecker.Category, highScore);

        highScorePanel.SetActive(false);
        gameOverButtons.SetActive(true);
    }

    private void HandleGameNotOver(GameOverChecker.GameNotOverEvent gameNotOverEvent)
        => Next();

    private void Next()
    {
        gameModeManager.HandleCheckerMode();
        nextPairPicker.GenerateNextPair();
    }

    private void HandleGameOver()
    {
        StopAllCoroutines();
        inputControls.Disable();

        var score = scoreKeeper.Score;
        var gameOverChecker = GameObjectUtils.GetComponentWithTag<GameOverChecker>("Manager");
        if (highScoresManager.CheckHighScore(gameOverChecker.Category, score))
        {
            highScorePanel.SetActive(true);
            var scoreField = highScorePanel.transform.Find("HighScoreFields/ScoreField");
            var scoreFieldText = scoreField.GetComponent<TMPro.TextMeshProUGUI>();
            scoreFieldText.SetText(score.ToString());
        }
        else
        {
            gameOverButtons.SetActive(true);
        }
    }
}
