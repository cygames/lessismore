using UnityEngine;

public abstract class Value : MonoBehaviour
{
    public abstract float GetValue();
}
