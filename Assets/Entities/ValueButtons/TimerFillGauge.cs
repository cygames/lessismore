using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectMask2D))]
public class TimerFillGauge : MonoBehaviour
{
    private RectMask2D rectMask;

    private float fullWidth;
    private float fullHeight;

    private void OnEnable()
    {
        CountDownTimer.OnTimerTick += OnTimerTick;

        rectMask = GetComponent<RectMask2D>();
    }

    private void OnDisable()
    {
        CountDownTimer.OnTimerTick -= OnTimerTick;
    }

    private void Start()
    {
        var countDownTimer = GameObjectUtils.GetComponentWithTag<CountDownTimer>("Manager");

        var rect = rectMask.rectTransform.rect;
        fullWidth = rect.width;
        fullHeight = rect.height;

        var multiplier = getMaskHeightMultiplier(countDownTimer.TimeRemaining, countDownTimer.TotalTime);
        rectMask.rectTransform.sizeDelta = new Vector2(fullWidth, multiplier * fullHeight);
    }

    private void OnTimerTick(CountDownTimer.TimerTickData data)
    {
        var multiplier = getMaskHeightMultiplier(data.TimeRemaining, data.TotalTime);
        rectMask.rectTransform.sizeDelta = new Vector2(fullWidth, fullHeight * multiplier);
    }

    private float getMaskHeightMultiplier(float timeRemaining, float totalTime)
    {
        return Math.Min(1f, timeRemaining / totalTime);
    }
}
