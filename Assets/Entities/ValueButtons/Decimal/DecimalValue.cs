using System;

public class DecimalValue : Value
{
    private const double MIN = 0.1;
    private const double MAX = 9.9;

    private float value;
    private TMPro.TextMeshProUGUI[] valueTexts;

    public DecimalValue()
    {
        var random = new Random((int) DateTime.Now.Ticks);
        var value = random.NextDouble() % (MAX - MIN) + MIN;
        this.value = (float)Math.Round(value, 1);
    }

    private void Start()
    {
        valueTexts = GetComponentsInChildren<TMPro.TextMeshProUGUI>();
        foreach (var valueText in valueTexts)
        {
            valueText.SetText(value.ToString());
        }
    }

    public override float GetValue()
    {
        return value;
    }
}
