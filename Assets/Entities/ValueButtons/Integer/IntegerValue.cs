using System;

public class IntegerValue : Value
{
    private const int MIN = 1;
    private const int MAX = 9;

    private int value;
    private TMPro.TextMeshProUGUI[] valueTexts;

    public IntegerValue()
    {
        var random = new Random((int) DateTime.Now.Ticks);
        value = random.Next(MIN, MAX);
    }

    private void Awake()
    {
        valueTexts = GetComponentsInChildren<TMPro.TextMeshProUGUI>();
        foreach (var valueText in valueTexts)
        {
            valueText.SetText(value.ToString());
        }
    }

    public override float GetValue()
    {
        return value;
    }
}
